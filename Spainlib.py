import json
import os
from shutil import copy

def read_json(fname):
    f = open(fname, "r")
    data = json.loads(f.read())

    return data

def copy_to_dir(src, dest, extrapath):
    for folder in dest:
        for f in os.listdir(src):
            copy(os.path.join(src, f), os.path.join(extrapath, folder)) # copy(src, dest) - from shutil

def copy_to_evidence(src, dest, evpath, fname):
    fullpath = os.path.join(src, fname)
    
    if (os.path.exists(fullpath)):
        for folder in dest:
            copy(fullpath, os.path.join(evpath,folder))



def create(flag):
    singular_license_path = os.path.join("Spain DVD", "Requisitos Tecnicos", "Singular License")
    SlotsPath = os.path.join("Spain DVD", "Requisitos Tecnicos", "Other Games - Slots")
    BLJpath = os.path.join("Spain DVD", "Requisitos Tecnicos", "Other Games - Blackjack")
    RLTpath = os.path.join("Spain DVD", "Requisitos Tecnicos", "Other Games - Roulette")
    POQpath = os.path.join("Spain DVD", "Requisitos Tecnicos", "Other Games - Poker")

    art_src = "art_ev"
    emu_src = "emu_ev"
    incomplete_src = "incomplete_ev"
    accounting_src = "accounting_ev"
    duration_src = "duration_ev"
    math_src = "math_ev"
    disable_src = "disable_ev"

    slots_ev = "slots_ev"
    blj_ev = "blj_ev"
    rlt_ev = "rlt_ev"
    poq_ev = "poq_ev"
    RES_TEC = "RES_TEC" # Source folder for the RES_TEC documents
    currdir = os.getcwd()

    testing_dest = read_json(os.path.join(currdir,"destinations.json"))


    # These stay constant for all types of games
    artwork_dest = testing_dest["artwork_folders"]
    emulation_dest = testing_dest["emulation_folders"]
    accounting_dest = testing_dest["accounting_folders"]
    incomplete_dest = testing_dest["incomplete_folders"] # Incomplete only in Singular License
    disable_dest = testing_dest["disable_folders"]
    math_dest = testing_dest["math_folders"]

    # These destinations change per game type (ie slots, blackjack etc.)
    if(flag == "OM_AZA"):
        artwork_ev = testing_dest["slots_ev"]["artwork_folders_ev"]
        emulation_ev = testing_dest["slots_ev"]["emulation_folders_ev"]
        accounting_ev = testing_dest["slots_ev"]["accounting_folders_ev"]
        duration_ev = testing_dest["slots_ev"]["duration_folders_ev"]
        math_ev = testing_dest["slots_ev"]["math_folders_ev"]
    
    if(flag == "OM_BLJ"):
        artwork_ev = testing_dest["blj_ev"]["artwork_folders_ev"]
        emulation_ev = testing_dest["blj_ev"]["emulation_folders_ev"]


    if(flag == "OM_RLT"):
        artwork_ev = testing_dest["rlt_ev"]["artwork_folders_ev"]
        accounting_ev = testing_dest["rlt_ev"]["accounting_folders_ev"]
    
    if(flag == "OM_POQ"):
        artwork_ev = testing_dest["poq_ev"]["artwork_folders_ev"]
        emulation_ev = testing_dest["poq_ev"]["emulation_folders_ev"]
        accounting_ev = testing_dest["poq_ev"]["accounting_folders_ev"]
        duration_ev = testing_dest["poq_ev"]["duration_folders_ev"]


    # These documents are constant for all games
    RES_TEC_313 = testing_dest["RES_TEC Annex I 3.13 Test Evidence"]
    RES_TEC_361 = testing_dest["RES_TEC Annex I 3.6.1 Test Evidence"]
    RES_TEC_391 = testing_dest["RES_TEC Annex I 3.9.1 Test Evidence"]
    RES_TEC_392 = testing_dest["RES_TEC Annex I 3.9.2 Test Evidence"]
    RES_TEC_394 = testing_dest["RES_TEC Annex I 3.9.4 Test Evidence"] 
    RES_TEC_395 = testing_dest["RES_TEC Annex I 3.9.5 Test Evidence"]

    # Other testing evidences will also need to go here if you have a RLT or BLJ game
    # In order to keep it minimal, we only have 1 .json file that has the locations of all destination folders
    # But for the testing documents we need to specify their destinations individually
    # Testing folders - you can add RLT, BLJ, etc.. here
    spainfolders = testing_dest["folders"]
    if(flag == "OM_AZA"):
        OM_AZA_folders = testing_dest["slots_ev"]["OM_AZA"]
        article812f_dest = testing_dest["slots_ev"]["OM_AZA Article 8.1.2(f) and 12.1 Test Evidence"]
        article812g_dest = testing_dest["slots_ev"]["OM_AZA Article 8.1.2(g) and 14.4 Test Evidence"]

    if(flag == "OM_BLJ"):
        OM_BLJ_folders = testing_dest["blj_ev"]["OM_BLJ"]
        article4first_dest = testing_dest["blj_ev"]["OM_BLJ Article 4 and Annex IV (1st) Test Evidence"]
        article4second_dest = testing_dest["blj_ev"]["OM_BLJ Article 4 and Annex IV (2nd) Test Evidence"]
        article4third_dest = testing_dest["blj_ev"]["OM_BLJ Article 4 and Annex IV (3rd) Test Evidence"]
        article4fourth_dest = testing_dest["blj_ev"]["OM_BLJ Article 4 and Annex IV (4th) Test Evidence"]
        article4fifth_dest = testing_dest["blj_ev"]["OM_BLJ Article 4 and Annex IV (5th) Test Evidence"]
        article121 = testing_dest["blj_ev"]["OM_BLJ Article 12.1 Test Evidence"]

    if(flag == "OM_RLT"):
        OM_RLT_folders = testing_dest["rlt_ev"]["OM_RLT"]
        article4first_dest = testing_dest["rlt_ev"]["OM_RLT Article 4 and Annex IV (1st) Test Evidence"]
        article4second_dest = testing_dest["rlt_ev"]["OM_RLT Article 4 and Annex IV (2nd) Test Evidence"]
        article121_dest = testing_dest["rlt_ev"]["OM_RLT Article 12.1 Test Evidence"]
        article145_dest = testing_dest["rlt_ev"]["OM_RLT Article 14.5 Test Evidence"]

    # Interestingly enough, poker uses the same documents as slots
    if(flag == "OM_POQ"):
        OM_POQ_folders = testing_dest["poq_ev"]["OM_POQ"]
        article812f_dest = testing_dest["poq_ev"]["OM_AZA Article 8.1.2(f) and 12.1 Test Evidence"]
        article812g_dest = testing_dest["poq_ev"]["OM_AZA Article 8.1.2(g) and 14.4 Test Evidence"]

    # Create the Entire Spain DVD folder structure
    if (not os.path.exists("Spain DVD")):
        for folder in spainfolders:
            os.makedirs(os.path.join(singular_license_path, folder))

        if(flag == "OM_AZA"):
            for folder in OM_AZA_folders:
                os.makedirs(os.path.join(SlotsPath, folder))
        
        if(flag == "OM_BLJ"):
            for folder in OM_BLJ_folders:
                os.makedirs(os.path.join(BLJpath, folder))
        
        if(flag == "OM_RLT"):
            for folder in OM_RLT_folders:
                os.makedirs(os.path.join(RLTpath, folder))
        
        if(flag == "OM_POQ"):
            for folder in OM_POQ_folders:
                os.makedirs(os.path.join(POQpath, folder))

    ## CONSTANT FOR ALL TYPES OF GAMES
    # copy_to_dir(src, dest, path)
    copy_to_dir(art_src, artwork_dest, singular_license_path)
    copy_to_dir(emu_src, emulation_dest, singular_license_path)
    copy_to_dir(accounting_src, accounting_dest, singular_license_path)
    copy_to_dir(incomplete_src, incomplete_dest, singular_license_path)
    copy_to_dir(disable_src, disable_dest, singular_license_path)
    copy_to_dir(math_src, math_dest, singular_license_path)
    

    # Copy the Spain DVD forms (Singular License Annex | thing)
    copy_to_evidence(RES_TEC, RES_TEC_313, singular_license_path, "RES_TEC Annex I 3.13 Test Evidence.docx")
    copy_to_evidence(RES_TEC, RES_TEC_361, singular_license_path, "RES_TEC Annex I 3.6.1 Test Evidence.docx")
    copy_to_evidence(RES_TEC, RES_TEC_391, singular_license_path, "RES_TEC Annex I 3.9.1 Test Evidence.docx")
    copy_to_evidence(RES_TEC, RES_TEC_392, singular_license_path, "RES_TEC Annex I 3.9.2 Test Evidence.docx")
    
    # The below are for 394 - BLJ, 395 - roulette, idk why they were included in the spreadshseet for all games
    copy_to_evidence(RES_TEC, RES_TEC_394, singular_license_path, "RES_TEC Annex I 3.9.4 Test Evidence.docx")
    copy_to_evidence(RES_TEC, RES_TEC_395, singular_license_path, "RES_TEC Annex I 3.9.5 Test Evidence.docx")

    # Copy files to Slots evidence
    # Copy OM_AZA evidences
    # No incomplete doc in OM_AZA folders
    if(flag == "OM_AZA"):
        copy_to_dir(art_src, artwork_ev, SlotsPath)
        copy_to_dir(emu_src, emulation_ev, SlotsPath)
        copy_to_dir(accounting_src, accounting_ev, SlotsPath)
        copy_to_dir(duration_src, duration_ev, SlotsPath)
        copy_to_dir(math_src, math_ev, SlotsPath)
        
        copy_to_evidence(slots_ev, article812f_dest, SlotsPath, "OM_AZA Article 8.1.2(f) and 12.1 Test Evidence.docx")
        copy_to_evidence(slots_ev, article812g_dest, SlotsPath, "OM_AZA Article 8.1.2(g) and 14.4 Test Evidence.docx")

    if(flag == "OM_BLJ"):
        copy_to_dir(art_src, artwork_ev, BLJpath)
        copy_to_dir(emu_src, emulation_ev, BLJpath)

        copy_to_evidence(blj_ev, article4first_dest, BLJpath, "OM_BLJ Article 4 and Annex IV (1st) Test Evidence.docx")
        copy_to_evidence(blj_ev, article4second_dest, BLJpath, "OM_BLJ Article 4 and Annex IV (2nd) Test Evidence.docx")
        copy_to_evidence(blj_ev, article4third_dest, BLJpath, "OM_BLJ Article 4 and Annex IV (3rd) Test Evidence.docx")
        copy_to_evidence(blj_ev, article4fourth_dest, BLJpath, "OM_BLJ Article 4 and Annex IV (4th) Test Evidence.docx")
        copy_to_evidence(blj_ev, article4fifth_dest, BLJpath, "OM_BLJ Article 4 and Annex IV (5th) Test Evidence.docx")
        copy_to_evidence(blj_ev, article121, BLJpath, "OM_BLJ Article 12.1 Test Evidence.docx")

    if(flag == "OM_RLT"):
        copy_to_dir(art_src, artwork_ev, RLTpath)
        copy_to_dir(accounting_src, accounting_ev, RLTpath) 

        copy_to_evidence(rlt_ev, article4first_dest, RLTpath, "OM_RLT Article 4 and Annex IV (1st) Test Evidence.docx")
        copy_to_evidence(rlt_ev, article4second_dest, RLTpath, "OM_RLT Article 4 and Annex IV (2nd) Test Evidence.docx")
        copy_to_evidence(rlt_ev, article121_dest, RLTpath, "OM_RLT Article 12.1 Test Evidence.docx")  
        copy_to_evidence(rlt_ev, article145_dest, RLTpath, "OM_RLT Article 14.5 Test Evidence")


    if(flag == "OM_POQ"):
        copy_to_dir(art_src, artwork_ev, POQpath)
        copy_to_dir(emu_src, emulation_ev, POQpath)
        copy_to_dir(accounting_src, accounting_ev, POQpath)
        copy_to_dir(duration_src, duration_ev, POQpath)
        
        copy_to_evidence(poq_ev, article812f_dest, POQpath, "OM_AZA Article 8.1.2(f) and 12.1 Test Evidence.docx")
        copy_to_evidence(poq_ev, article812g_dest, POQpath, "OM_AZA Article 8.1.2(g) and 14.4 Test Evidence.docx")