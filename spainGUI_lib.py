import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import PyQt5

from SpainDVD import Create_DVD
from misc import *
from Spainlib import read_json

import os

# from PyQt5.QtWidgets import QApplication, QLabel # This throws a false error
from PyQt5 import QtWidgets

class Window(QtWidgets.QWidget):
    def __init__(self, parent = None):
        super(Window, self).__init__(parent)
        
        self.resize(400, 400)

        # Here is your window layout - we're using Grid
        # Below, add the widgets to the layout, then set the layout at the end
        windowLayout = QtWidgets.QGridLayout()

        self.start_button = QtWidgets.QPushButton("Start")
        self.slots_radio = QtWidgets.QRadioButton("Other Games - Slots (OM_AZA)")
        self.slots_radio.setChecked(True)
        self.blj_radio = QtWidgets.QRadioButton("Other Games - Blackjack (OM_BLJ)")
        self.rlt_radio = QtWidgets.QRadioButton("Other Games - Roulette (OM_RLT)")
        self.poq_radio = QtWidgets.QRadioButton("Other Games - Poker (OM_POQ)")

        windowLayout.addWidget(self.slots_radio, 0, 1)
        windowLayout.addWidget(self.blj_radio, 1, 1)
        windowLayout.addWidget(self.rlt_radio, 2, 1)
        windowLayout.addWidget(self.poq_radio, 3, 1)

        windowLayout.addWidget(self.start_button, 4, 1)
        self.setLayout(windowLayout)

        source_folders = read_json("destinations.json")
        self.folders = source_folders["evidence_source"]

        # Create the folders from where you will copy the evidence over to the DVD
        for ff in self.folders:
            if (not os.path.exists(ff)):
                os.makedirs(ff)

        self.start_button.clicked.connect(self.on_click)

    # Runs when you push the start button!
    def on_click(self):

        self.flag = "UNSET" # primitive, but necessary

        if (self.slots_radio.isChecked()):
            self.flag = "OM_AZA"

        if (self.blj_radio.isChecked()):
            self.flag = "OM_BLJ"

        if (self.rlt_radio.isChecked()):
            self.flag = "OM_RLT"
        
        if (self.poq_radio.isChecked()):
            self.flag = "OM_POQ"
        

        # If nothing is selected, throw a pop up error and return
        if (self.flag == "UNSET"):
            self.dlg = PopUp(self)
            self.dlg.pop_up_text("Nothing selected!")
            self.dlg.exec()
            return
        else:
            pass

        Create_DVD(self.flag)
        self.confirm = PopUp(self)
        self.confirm.pop_up_text("Spain DVD created!")
        self.confirm.exec()



