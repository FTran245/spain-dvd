
import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import PyQt5
from PyQt5 import QtWidgets

# Pop up error handling template
class PopUp(QtWidgets.QDialog):
    def __init__(self, *args, **kwargs):
        super(PopUp, self).__init__(*args, **kwargs)

        self.setWindowTitle("Attention!") # This is the window title, not the dispalyed error message

        QBtn = QtWidgets.QDialogButtonBox.Ok 
        
        self.buttonBox = QtWidgets.QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

    # This 'self' refers to the self of the pop up box, not the original for we are filling out, it's weird like that
    def pop_up_text(self, text):
        self.label = QtWidgets.QLabel(text) # Set the error message text
        self.label.setAlignment(Qt.AlignCenter) # This is a false error, but it aligns your text

        self.layout = QtWidgets.QVBoxLayout()
        self.layout.addWidget(self.label)
        self.layout.addWidget(self.buttonBox)
        self.setLayout(self.layout)

