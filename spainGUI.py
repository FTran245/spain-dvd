import sys
from PyQt5 import QtCore

# from PyQt5.QtWidgets import QApplication, QLabel # This throws a false error
from PyQt5 import QtWidgets

from spainGUI_lib import *

if __name__ == '__main__':
    SpainApp = QtWidgets.QApplication(sys.argv)
    SpainApp.setStyle("Fusion")
    
    UI = Window()
    UI.show()
    SpainApp.exec()
